<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHasWorkPlan extends Model
{
    protected $table = 'user_has_work_plans';

    protected $guarded = [];
}
