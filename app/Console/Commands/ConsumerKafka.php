<?php

namespace App\Console\Commands;

use App\Models\UserHasWorkPlan;

class ConsumerKafka extends AbstractConsumer
{
    protected $signature = 'app:consumer-kafka';

    protected $description = 'Command description';

    protected function handleMessage($message): void
    {
        $message = $message['payload'];

        $type = $this->getType($message);

        $this->info($type . ' data');

        $id = $this->getId($message, $type);

        UserHasWorkPlan::updateOrCreate(
            [
                'work_plan_id' => $id,
                'type' => $type
            ],
            [
                'data_before' => $message['before'] ? json_encode($message['before']) : '',
                'data_after' => $message['after'] ? json_encode($message['after']) : '',
            ]
        );

        $this->info('Done ' . $type . ' data');
    }

    private function getType($message): string
    {
        return match ($message['op']) {
            'c' => 'Create',
            'u' => 'Update',
            'd' => 'Delete',
            default => 'Read',
        };
    }

    private function getId($message, $type): string
    {
        return match ($type) {
            'Create',
            'Update' => $message['after']['id'],
            'Delete' => $message['before']['id']
        };
    }

    protected function getTopic(): string
    {
        return config('kafka.topics.work_plan');
    }

    protected function getGroupId(): string
    {
        return config('kafka.consumer_group_id.work_plan');
    }

    protected function getSchemaName(): string
    {
        return config('kafka.schema_avro.work-plan');
    }
}
