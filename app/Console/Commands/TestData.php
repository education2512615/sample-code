<?php

namespace App\Console\Commands;

use App\DesignPattern\COR\CalculateDistance;
use App\DesignPattern\COR\Point;
use Illuminate\Console\Command;

class TestData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle(): void
    {
        $calculateDistance = app()->make(CalculateDistance::class);

        $source = new Point(1, 2);
        $destination = new Point(3, 4);

        $distance = $calculateDistance->handle($source, $destination);

        $this->info("Distance: $distance");
    }
}
