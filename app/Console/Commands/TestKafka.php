<?php

namespace App\Console\Commands;

use Carbon\Exceptions\Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Junges\Kafka\Exceptions\KafkaConsumerException;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Contracts\KafkaConsumerMessage;

class TestKafka extends Command
{
    protected $signature = 'app:test-consumer-kafka';

    protected $description = 'Command test consumer kafka';


    /**
     * @throws Exception
     * @throws KafkaConsumerException
     */
    public function handle(): void
    {
        $consumer = Kafka::createConsumer([$this->getTopic()], $this->getGroupId(), $this->getBrokers())
            ->withHandler(function (KafkaConsumerMessage $message) {
                try {
                    $this->line(json_encode($message->getBody()));
                } catch (\Exception $e) {
                    Log::error('Error decoding JSON:', (array)$e->getMessage());
                }

            })->build();

        $consumer->consume();
    }

    private function getTopic(): string
    {
        return config('kafka.topics.work_plan');
    }

    private function getGroupId(): string
    {
        return config('kafka.consumer_group_id.work_plan');
    }

    private function getBrokers(): string
    {
        return config('kafka.brokers');
    }
}
