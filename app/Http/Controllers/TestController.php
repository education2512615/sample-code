<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use const Widmogrod\Monad\State\value;

class TestController extends Controller
{
    public function enQueue(Request $request)
    {
        $queue = $request->queue;
        $data = $request->data;

        $timeout = 30; // 3 seconds timeout
        Redis::rpush($queue, json_encode($data));
        Redis::expire($queue, $timeout);

        return response()->json([
            'status' => 'OK',
            'message' => 'Data pushed to redis'
        ]);
    }

    public function deQueue(Request $request)
    {
        $queue = $request->queue;
        $lockKey = "{$queue}:lock";
        $lockTimeout = 30; // Timeout for lock
        $lockAcquired = Redis::setnx($lockKey, time() + $lockTimeout);
        $data = [];
        if ($lockAcquired || time() > Redis::get($lockKey)) {
            Redis::set($lockKey, time() + $lockTimeout);

            $job = Redis::lpop($queue);

            if ($job) {
                try {
                    $data = json_decode($job, true);

                } finally {
                    Redis::del($lockKey);
                }
            } else {
                Redis::del($lockKey);
            }
        }

        return response()->json([
            'status' => 'OK',
            'data' => $data
        ]);
    }
}
