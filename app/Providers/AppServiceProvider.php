<?php

namespace App\Providers;

use App\DesignPattern\COR\AbstractDistance;
use App\DesignPattern\COR\CalculateByBike;
use App\DesignPattern\COR\CalculateByBird;
use App\DesignPattern\COR\CalculateByCar;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AbstractDistance::class, function () {
            return new CalculateByCar(function () {
                return new CalculateByBike(function () {
                    return new CalculateByBird();
                });
            });
        });

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
