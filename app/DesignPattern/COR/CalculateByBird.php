<?php

namespace App\DesignPattern\COR;

class CalculateByBird extends AbstractDistance
{
    protected function calculate(Point $pointA, Point $pointB): float
    {
        return 10;
    }
}
