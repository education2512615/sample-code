<?php

namespace App\DesignPattern\COR;

class CalculateDistance
{
    public function __construct(public readonly AbstractDistance $abstractDistance)
    {
    }

    public function handle(Point $source, Point $destination): int
    {
       return $this->abstractDistance->handle($source, $destination);
    }

}
