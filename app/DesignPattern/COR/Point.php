<?php

namespace App\DesignPattern\COR;

class Point
{
    public function __construct(public float $lat, public float $lon)
    {
    }
}
