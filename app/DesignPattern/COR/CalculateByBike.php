<?php

namespace App\DesignPattern\COR;

class CalculateByBike extends AbstractDistance
{
    protected function calculate(Point $pointA, Point $pointB): float
    {
        return 50;
    }
}
