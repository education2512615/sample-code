<?php

namespace App\DesignPattern\COR;

abstract class AbstractDistance
{
    public $nextHandler;

    public function __construct(callable $nextHandler = null)
    {
        $this->nextHandler = $nextHandler;
    }

    abstract protected function calculate(Point $pointA, Point $pointB): float;

    public function handle(Point $source, Point $destination): int
    {
        try {
            //tinh toan khoang cach
            return $this->calculate($source, $destination);

        } catch (\Exception $exception) {

            if ($this->nextHandler === null) {
                throw new \LogicException('Not found');
            }

            return $this->next()->handle($source, $destination);
        }
    }

    protected function next()
    {
        return call_user_func($this->nextHandler);
    }

}
