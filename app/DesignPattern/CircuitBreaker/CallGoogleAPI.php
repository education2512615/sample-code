<?php

namespace App\DesignPattern\CircuitBreaker;

use Ackintosh\Ganesha\Builder;
use Ackintosh\Ganesha\Storage\Adapter\Redis;

class CallGoogleAPI
{

    public function hanlde(string $key): int|string
    {
        $ganesha = Builder::withRateStrategy()
            ->adapter(new Redis(''))
            ->failureRateThreshold(50)
            ->intervalToHalfOpen(3)
            ->minimumRequests(10)
            ->timeWindow(5)
            ->build();


        if (!$ganesha->isAvailable($key)) {
           return 'Dich vu khong kha dung';
        }

        try {
            $response =  $this->callGoogleAPIToCalculateDistance();
            $ganesha->success($key);
            return $response;
        } catch (\Exception $e) {
            report($e);
            $ganesha->failure($key);
            return 0;
        }
    }

    private function callGoogleAPIToCalculateDistance(): string
    {
        return 'Google API called';
    }
}
