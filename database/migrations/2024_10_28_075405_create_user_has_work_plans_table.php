<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_has_work_plans', function (Blueprint $table) {
            $table->id();
            $table->integer('work_plan_id')->index();
            $table->text('data_before')->nullable(true);
            $table->text('data_after')->nullable(true);
            $table->string('type')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_has_work_plans');
    }
};
